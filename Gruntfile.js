module.exports = function(grunt) {

    grunt.initConfig({

        // package.json
        pkg: grunt.file.readJSON('package.json'),

        // Переменные каталогов проекта
        project: {
            name: ['ecomarket'],
            app: ['public'],
            dist: ['<%= project.app %>/dist'],
            scss: ['<%= project.app %>/<%= project.name %>.scss'],
            css: ['<%= project.dist %>/sass/main.scss'],
            libs: ['<%=project.app %>/libs'],
            js: ['<%= project.dist %>/js'],
            blocks: ['<%= project.app %>/blocks'],
        },

        // Настройки сервера
        express: {
            dev: {
                options: {
                    script: 'server.js'
                }
            }
        },

        // Склеивание JS файлов
        concat: {
            base: {
                src: [
                    '<%= project.libs %>/js/*.js'
                ],
                dest: '<%= project.js %>/<%= project.name %>-base.js'                  // production
            },
            run: {
                src: '<%= project.blocks %>/**/js/*.js',
                dest: '<%= project.js %>/<%= project.name %>-run.js'
            }
        },

        // Сжатие общего JS файла
        uglify: {
            // options: {
            //     banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            // },
            build: {
                src: '<%= project.js %>/build/production.js',                  // production
                dest: '<%= project.js %>/build/production.min.js'              // production min
            }
        },

        // SASS
        sass: {
            dist: {
                options: {
                    style: 'expanded',
                    //require: 'susy'
                },
                files: {
                    '<%= project.dist %>/css/<%= project.name %>.css': '<%= project.scss %>'
                }
            }
        },

        // Autoprefixer
        autoprefixer: {

            options: {
                browsers: ['> 1%', 'last 2 versions', 'ie 9']
            },

            single_file: {
                src: '<%= project.assets %>/css/build/allscss.css',
                dest: '<%= project.assets %>/css/build/allscss-autoprefixer.css'
            }

        },

        // Минимизация CSS
        cssmin: {
            combine: {
                files: {
                    '<%= project.app %>/css/build/production.min.css': [
                        '<%= project.assets %>/css/build/allscss-autoprefixer.css'
                    ]
                }
            }
        },

        // Слежение за изменениями
        watch: {
            css: {
                files: [
                    '<%= project.app %>/blocks/**/{,*/}*.{scss,sass}'
                ],
                tasks: ['sass'],
                options: {
                    spawn: false,
                }
            },
            scripts: {
                files: [
                    '<%= project.libs %>/js/*.js',
                    '<%= project.blocks %>/**/js/*.js'
                ],
                tasks: ['concat'],
                options: {
                    spawn: false
                }
            }
        }

    });

    // Загрузка модулей, которые предварительно установлены
    grunt.loadNpmTasks('grunt-express-server');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Эти задания будут выполнятся сразу после команды grunt
    grunt.registerTask('default', ['express', 'concat', 'sass', 'watch']);

};