function setCatalogueBehavior() {
	var options = {
		block: '.js-catalogue',
		links: '.js-catalogue-add',
		modal: '.js-catalogue-modal'
	}

	var $block = $(options.block);
	var $links = $block.find(options.links);
	var $modal = $block.find(options.modal);



	$links.on('click', function(e) {
		e.preventDefault();

		var $parent = $(this).parent();
		$links.parent().not($parent).removeClass('active');

		$parent.toggleClass('active');
	});

	$(document).click(function(e) {
		var $el = $(event.target);

		if ($el.closest(options.block).length) return;

		$($links.parent()).removeClass('active');

        e.stopPropagation();
	});

	$('.js-scroll a').on('click', function(e) {
		e.preventDefault();
		$(this).toggleClass('ok');
	})

	$modal.find('.js-scroll').jScrollPane();
};
function counter() {
	var options = {
		block: '.js-counter'
	}

	$('.js-counter-start').on('click', function() {
		var $elem = $(this);
		var $parent = $elem.closest(options.block);


		$parent.addClass('active');
		$elem.css({
			left: $elem.offset().left,
			top: $elem.offset().top,
			position: 'fixed'
		})
		$elem
			.animate({
				left: 1120,
				top: 20
			}, 900, function() {
				$elem.attr('style', '');
			}
		);


		counter.init($parent.find('.js-counter-pane'));

		return false;
	});

	var counter = {
		init: function(pane) {
			this.$pane = pane;
			this.$plus = this.$pane.children('.js-counter-plus');
			this.$minus = this.$pane.children('.js-counter-minus');
			this.$viewport = this.$pane.children('span');

			this.count = +this.$viewport.text();

			this.setBindings();
		},
		setBindings: function() {
			var self = this;
			this.$plus.on('click', function(e) {
				e.preventDefault();

				self.$viewport.text(self.inc());
			});
			this.$minus.on('click', function(e) {
				e.preventDefault();

				self.count > 1
					? self.$viewport.html(self.dec())
					: self.stopCounter();
			})
		},
		inc: function() {
			return this.count += 1;
		},
		dec: function() {
			return this.count -= 1;
		},
		stopCounter: function() {
			this.$plus.off('click');
			this.$minus.off('click');
			this.$pane.parent().removeClass('active');
		}
	}

	function slideElem(el, leftLimit, topLimit) {
		var $elem = el;
		var coords = {};
		slideInit();

		function slideInit() {

			coords.left = $elem.offset().left;
			coords.top = $elem.offset().top;

			console.log(coords.left)

			$elem.css({
				left: coords.left,
				top: coords.top,
				position: 'fixed'
			});

			slideRun();
		}
		function slideRun() {
			var timer = setInterval(function() {
				coords.left = $elem.offset().left;
				coords.top = $elem.offset().top;

				if(coords.left < leftLimit && coords.top < topLimit) {
					$elem.css({
						left: coords.left + 1,
						top: coords.top + -1
					});
				}
				else {
					clearInterval(timer);
				}
			}, 1);
		}
	}
};
(function() {
    showMenu();

	$(document).ready(function() {
	    $(".various").fancybox({
	        fitToView   : false,
	        width       : '70%',
	        height      : '70%',
	        autoSize    : false,
	        closeClick  : false,
	        openEffect  : 'none',
	        closeEffect : 'none'
	    });
	});




    $(window).scroll(function() {
        $('#object').each(function(){
        var imagePos = $(this).offset().top;

        var topOfWindow = $(window).scrollTop();
            if (imagePos < topOfWindow+900) {
                $(this).addClass("slideRight");
            }
        });
    });

        $(window).scroll(function() {
        $('.menu').each(function(){
        var imagePos = $(this).offset().top;

        var topOfWindow = $(window).scrollTop();
            if (imagePos < topOfWindow+900) {
                $(this).addClass("fadeIn");
            }
        });
    });
        $(window).scroll(function() {
        $('.novelty').each(function(){
        var imagePos = $(this).offset().top;

        var topOfWindow = $(window).scrollTop();
            if (imagePos < topOfWindow+900) {
                $(this).addClass("fadeIn");
            }
        });
    });
    $(window).scroll(function() {
        $('.salats').each(function(){
        var imagePos = $(this).offset().top;

        var topOfWindow = $(window).scrollTop();
            if (imagePos < topOfWindow+900) {
                $(this).addClass("fadeIn");
            }
        });
    });
    $(window).scroll(function() {
        $('.hot').each(function(){
        var imagePos = $(this).offset().top;

        var topOfWindow = $(window).scrollTop();
            if (imagePos < topOfWindow+900) {
                $(this).addClass("fadeIn");
            }
        });
    });
    $(window).scroll(function() {
        $('.soups').each(function(){
        var imagePos = $(this).offset().top;

        var topOfWindow = $(window).scrollTop();
            if (imagePos < topOfWindow+900) {
                $(this).addClass("fadeIn");
            }
        });
    });
    $(window).scroll(function() {
        $('.napitki').each(function(){
        var imagePos = $(this).offset().top;

        var topOfWindow = $(window).scrollTop();
            if (imagePos < topOfWindow+900) {
                $(this).addClass("fadeIn");
            }
        });
    });
    $(window).scroll(function() {
        $('.deserts').each(function(){
        var imagePos = $(this).offset().top;

        var topOfWindow = $(window).scrollTop();
            if (imagePos < topOfWindow+900) {
                $(this).addClass("fadeIn");
            }
        });
    });
    $(window).scroll(function() {
        $('#sl-l').each(function(){
        var imagePos = $(this).offset().top;

        var topOfWindow = $(window).scrollTop();
            if (imagePos < topOfWindow+900) {
                $(this).addClass("hatch");
            }
        });
    });
    $(window).scroll(function() {
        $('#sl-up').each(function(){
        var imagePos = $(this).offset().top;

        var topOfWindow = $(window).scrollTop();
            if (imagePos < topOfWindow+900) {
                $(this).addClass("slideDown");
            }
        });
    });
        $(window).scroll(function() {
        $('#lovework i').each(function(){
        var imagePos = $(this).offset().top;

        var topOfWindow = $(window).scrollTop();
            if (imagePos < topOfWindow+900) {
                $(this).addClass("fadeIn");
            }
        });
    });
        $(window).scroll(function() {
        $('#lovework .box span').each(function(){
        var imagePos = $(this).offset().top;

        var topOfWindow = $(window).scrollTop();
            if (imagePos < topOfWindow+900) {
                $(this).addClass("slideLeft");
            }
        });
    });
        $(window).scroll(function() {
        $('#lovework .box small').each(function(){
        var imagePos = $(this).offset().top;

        var topOfWindow = $(window).scrollTop();
            if (imagePos < topOfWindow+900) {
                $(this).addClass("slideRight");
            }
        });
    });

})();


(function() {
	$(document).ready(function() {
	    $('.menutop ul li').click(function() {
	        $.arcticmodal({
	            type: 'ajax',
	            url: $(this).children('a').attr('href'),
	            afterLoading: function(data, el) {
	                $(".arcticmodal-container").css({
	                	top: screen.height,
	                	overflow: 'hidden'
	                });

	            },
	            afterLoadingOnShow: function(data, el) {
	                $(".arcticmodal-container").animate({
	                    top: "-=" + screen.height,
	                }, 1500, function() {
	                	$(".arcticmodal-container").css({
		                	overflow: 'auto'
		                });
	                });

	                bindCloseModal();
	                setCatalogueBehavior();
	                bindSelect();
	                counter();
	                showMenu();
	            },
	            beforeClose: function(data, el) {
	            	$(".arcticmodal-container").css({ overflow: 'hidden' });
	                $(".arcticmodal-container").animate({
	                    top: "+=" + screen.height,
	                }, 1500);
	            }
	        });
	    });
	});
	function bindCloseModal() {
		$('.js-close_modal').on('click', function() {
        	$('.arcticmodal-overlay').trigger('click');
    	});
	}
})();


function showMenu() {
	var $block = $('.b-nav');
	var $links = $block.find('a');

	$links.on('click', function(e) {
		e.preventDefault();
	});

	$(window).on('scroll', function() {

		var top = $(document).scrollTop();
		if (top > 268)
			$('.menutop').addClass('fixed');
		else
			$('.menutop').removeClass('fixed');
	});

	$('.arcticmodal-container').on('scroll', function() {
		var top = $(this).scrollTop();
		if (top > 268) {
			$('.menutop').addClass('fixed');
			$('.js-fix').addClass('fixed');
		}
		else {
			$('.menutop').removeClass('fixed');
			$('.js-fix').removeClass('fixed');
		}
	});
};

function bindSelect() {
	var $select = $('.js-select').find('select');
	$select.select2({
		minimumResultsForSearch: -1
	});
};
(function() {

	var options = {
		stopOnHover: true,
		autoPlay: 5000,
		slideSpeed : 500,
		paginationSpeed : 500,
		items : 1,
		itemsDesktop : false,
		itemsDesktopSmall : false,
		itemsTablet: false,
		itemsMobile : false
  	}

  	var $block = $(".js-slider");
  	$.each($block, function() {
  		var $el = $(this);
  		var settings = $el.data('slider-settings');
  		console.log($.extend({}, options, settings));
  		$el.owlCarousel($.extend({}, options, settings));
  	})




})();