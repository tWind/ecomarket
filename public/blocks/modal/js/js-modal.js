(function() {
	$(document).ready(function() {
	    $('.menutop ul li').click(function() {
	        $.arcticmodal({
	            type: 'ajax',
	            url: $(this).children('a').attr('href'),
	            afterLoading: function(data, el) {
	                $(".arcticmodal-container").css({
	                	top: screen.height,
	                	overflow: 'hidden'
	                });

	            },
	            afterLoadingOnShow: function(data, el) {
	                $(".arcticmodal-container").animate({
	                    top: "-=" + screen.height,
	                }, 1500, function() {
	                	$(".arcticmodal-container").css({
		                	overflow: 'auto'
		                });
	                });

	                bindCloseModal();
	                setCatalogueBehavior();
	                bindSelect();
	                counter();
	                showMenu();
	            },
	            beforeClose: function(data, el) {
	            	$(".arcticmodal-container").css({ overflow: 'hidden' });
	                $(".arcticmodal-container").animate({
	                    top: "+=" + screen.height,
	                }, 1500);
	            }
	        });
	    });
	});
	function bindCloseModal() {
		$('.js-close_modal').on('click', function() {
        	$('.arcticmodal-overlay').trigger('click');
    	});
	}
})();

