(function() {

	var options = {
		stopOnHover: true,
		autoPlay: 5000,
		slideSpeed : 500,
		paginationSpeed : 500,
		items : 1,
		itemsDesktop : false,
		itemsDesktopSmall : false,
		itemsTablet: false,
		itemsMobile : false
  	}

  	var $block = $(".js-slider");
  	$.each($block, function() {
  		var $el = $(this);
  		var settings = $el.data('slider-settings');
  		console.log($.extend({}, options, settings));
  		$el.owlCarousel($.extend({}, options, settings));
  	})




})();