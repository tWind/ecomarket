function bindSelect() {
	var $select = $('.js-select').find('select');
	$select.select2({
		minimumResultsForSearch: -1
	});
};