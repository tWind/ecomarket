function counter() {
	var options = {
		block: '.js-counter'
	}

	$('.js-counter-start').on('click', function() {
		var $elem = $(this);
		var $parent = $elem.closest(options.block);


		$parent.addClass('active');
		$elem.css({
			left: $elem.offset().left,
			top: $elem.offset().top,
			position: 'fixed'
		})
		$elem
			.animate({
				left: 1120,
				top: 20
			}, 900, function() {
				$elem.attr('style', '');
			}
		);


		counter.init($parent.find('.js-counter-pane'));

		return false;
	});

	var counter = {
		init: function(pane) {
			this.$pane = pane;
			this.$plus = this.$pane.children('.js-counter-plus');
			this.$minus = this.$pane.children('.js-counter-minus');
			this.$viewport = this.$pane.children('span');

			this.count = +this.$viewport.text();

			this.setBindings();
		},
		setBindings: function() {
			var self = this;
			this.$plus.on('click', function(e) {
				e.preventDefault();

				self.$viewport.text(self.inc());
			});
			this.$minus.on('click', function(e) {
				e.preventDefault();

				self.count > 1
					? self.$viewport.html(self.dec())
					: self.stopCounter();
			})
		},
		inc: function() {
			return this.count += 1;
		},
		dec: function() {
			return this.count -= 1;
		},
		stopCounter: function() {
			this.$plus.off('click');
			this.$minus.off('click');
			this.$pane.parent().removeClass('active');
		}
	}

	function slideElem(el, leftLimit, topLimit) {
		var $elem = el;
		var coords = {};
		slideInit();

		function slideInit() {

			coords.left = $elem.offset().left;
			coords.top = $elem.offset().top;

			console.log(coords.left)

			$elem.css({
				left: coords.left,
				top: coords.top,
				position: 'fixed'
			});

			slideRun();
		}
		function slideRun() {
			var timer = setInterval(function() {
				coords.left = $elem.offset().left;
				coords.top = $elem.offset().top;

				if(coords.left < leftLimit && coords.top < topLimit) {
					$elem.css({
						left: coords.left + 1,
						top: coords.top + -1
					});
				}
				else {
					clearInterval(timer);
				}
			}, 1);
		}
	}
};