function showMenu() {
	var $block = $('.b-nav');
	var $links = $block.find('a');

	$links.on('click', function(e) {
		e.preventDefault();
	});

	$(window).on('scroll', function() {

		var top = $(document).scrollTop();
		if (top > 268)
			$('.menutop').addClass('fixed');
		else
			$('.menutop').removeClass('fixed');
	});

	$('.arcticmodal-container').on('scroll', function() {
		var top = $(this).scrollTop();
		if (top > 268) {
			$('.menutop').addClass('fixed');
			$('.js-fix').addClass('fixed');
		}
		else {
			$('.menutop').removeClass('fixed');
			$('.js-fix').removeClass('fixed');
		}
	});
};
